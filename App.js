import React,{ useState} from 'react';
import { StyleSheet, Text, TextInput, View,KeyboardAvoidingView,Button, Keyboard, Pressable, TouchableOpacity } from 'react-native';
import Task from './components/Task';

export default function App() {
  const [task,setTask] = useState();
  const [taskItems, setTaskItems] = useState([]);

  const addTask = ()=>{
    if(task.trim()!== ''){
    Keyboard.dismiss();
    setTaskItems([...taskItems,task]);
    setTask('');
    }
  }
  const deleteTask = (index) =>{
    let items = [...taskItems];
    items.splice(index,1);
    setTaskItems(items);
  }
  return (
    <View style={styles.container}>
      
      <View style={styles.taskWrapper}>
        <Text style={styles.title}>Lista de tarefas</Text>
      </View>
      <KeyboardAvoidingView style={styles.writeTask}>
      <TextInput style={styles.input} 
      placeholder={'Digite uma tarefa'}
      value={task}
      onChangeText={text => setTask(text)} />
      </KeyboardAvoidingView>
      <Pressable style={styles.button} onPress={()=>addTask()} >
        <Text style={styles.buttonText}>Adicionar Tarefa</Text>
      </Pressable>
      {/*lista de tarefas*/}
      <View style={styles.item}>
        {
          taskItems.map((cont,index)=>{
            return(
              <Pressable key={index} onPress={()=>deleteTask(index)}>
                <Task  text={cont}/>
              </Pressable>
            ) 
          })
        }
      </View>
      
      
     
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E8EAED',
  },
  taskWrapper:{
    paddingTop:80,
    paddingBottom:20,
  },
  title:{
    fontSize:24,
    fontWeight:'bold',
  },
  item:{
    marginTop:30,
  },
  writeTask:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
  },
  input:{
    paddingVertical:15,
    paddingHorizontal:15,
    width:500,
    backgroundColor:'#FFF',
  },
  button:{
    paddingVertical:15,
    paddingHorizontal:15,
    width:250,
    backgroundColor:'#33FF4F'
  },
  buttonText:{
    textAlign:'center',
  }
});
